import React, { Component } from 'react';
import './App.css';
import ChartComponent from './chartComponent'
import CSVParserComponent from './CSVParserComponent'
class App extends Component {
  render() {
    return (
      <div className="App">
        <ChartComponent name="chandu"/>
        <  CSVParserComponent/>
      </div>
    );
  }
}

export default App;
