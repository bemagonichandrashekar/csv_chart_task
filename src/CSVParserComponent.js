import React from 'react';
import ReactFileReader from 'react-file-reader-input';
import ChartComponent from './chartComponent'
import './index.css';
import './App.css';
class CSVParserComponent extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            options: {},
            csv_result: []
        };

        this.handleFiles = this.handleFiles.bind(this);
        this.csvParse = this.csvParse.bind(this);

    }

    /**
     *
     * @param data
     */
    csvParse = (data) => {
        let series_data = [];
        const series_data_new_years = [];
        for (let data_number = 0; data_number < data.length; data_number++) {
            const temp_data = data[data_number].data;
            const tempYears = Object.keys(temp_data);
            const tempYears_int = tempYears.map(function(item) {
                return parseInt(item, 10);
            });
            for (let a = 0; a < tempYears_int.length; a++) {
                series_data_new_years.push(tempYears_int[a]);
            }
            series_data.push({
                name: data[data_number].name,
                data_values_years:data[data_number]
            })
        }

        const uniqueArray = series_data_new_years.filter(function(item, pos) {
            return series_data_new_years.indexOf(item) === pos;
        });

        const uniqueArraySorted = uniqueArray.sort();
        for (let s = 0; s < series_data.length; s++) {
            let tempValues =[];
            const temp_data_values_years =series_data[s].data_values_years;
            for (let n=0; n<uniqueArraySorted.length; n++){
                if(temp_data_values_years.data[uniqueArraySorted[n]]!== undefined){
                    tempValues.push(parseInt(temp_data_values_years.data[uniqueArraySorted[n]]))

                }else{
                    tempValues.push(null)
                }
            }
            series_data[s].data = tempValues;
        }
        const options_data = {
            title: {
                text: 'Task'
            },
            xAxis: {
                title: {
                    text: '<b>Years</b>'
                }
            },
            yAxis: {
                title: {
                    text: '<b>Values</b>'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: uniqueArraySorted[0]
                }
            },
            series: series_data,
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        };

        this.setState({options: options_data})
    };

    /**
     *
     * @param e
     * @param results
     */
    handleFiles = (e, results) => {
        const self = this;
        results.forEach(result => {

            const [e, file] = result;
            console.log(e);
            // let filename=files && files[0] && files[0].name
            let filename = file && file && file.name;
            let filetype = filename.split('.')[1];
            let fileindex = filename.indexOf('.csv');
            if (file.type === "text/csv" || filetype === 'csv' || fileindex > -1) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    let lines = reader.result.split("\n");
                    let resultArray = [];
                    for (let lineNumber = 0; lineNumber < lines.length; lineNumber++) {
                        if (lines[lineNumber] !== '') {
                            let tempRow = lines[lineNumber].split(",");
                            let temSeries = {name: "", data:{} };
                            temSeries.name = tempRow[0];
                            for (let temSeriesNumber = 1; temSeriesNumber < tempRow.length; temSeriesNumber++) {
                                const seriesString = tempRow[temSeriesNumber];
                                const seriesArray = seriesString.split('|');
                                temSeries.data[seriesArray[0]]= seriesArray[1];
                            }
                            resultArray.push(temSeries);
                        }

                    }
                    self.setState({csv_result: resultArray});
                    self.csvParse(resultArray);

                }
                reader.readAsText(file);

            } else {
                alert("File not supported")
            }
        });
    }

    render() {
        return <div > <div className='App-header'>
            <header ><h2>Converting CSV file data and displaying</h2></header>

        </div>
            <div>
            <ReactFileReader onChange={this.handleFiles} accept=".csv"
                            >
                <button className='btn btn-upload-config'>Upload</button>
                <div style={{'fontSize': '10px'}}> (CSV file only)
                </div>
            </ReactFileReader>
            </div>
                <div className='App-body' >
                    <ChartComponent options_data1="dd" options_data={JSON.stringify(this.state.options)}/>
                </div>
        </div>
    }
}

export default CSVParserComponent;