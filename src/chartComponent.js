import React from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

class ChartComponent extends React.Component {

    render() {
        return <div> {this.props.options_data && this.props.options_data !== '{}' && <HighchartsReact
            highcharts={Highcharts}
            options={JSON.parse(this.props.options_data)}
        />}

        </div>
    }
}

export default ChartComponent;